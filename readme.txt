------------
 Portile
 v0.3

by Stephen Lavelle/Increpare
analytic@gmail.com
http://www.maths.tcd.ie/~icecube
http://forums.tigsource.com/index.php?topic=2161.0

------------

Additions in this version:
	- Sound effects
	- Archimediam


contains 25 levels

----------
-controls-
----------

LEFT/RIGHT : move left/right

W/S/A/D=fire portal opening A in a particular direction  (can only have portal launching at any given moment)

SHIFT + W/S/A/D=fire portal opening B in a particular direction

E = closes both portal openings

automatic respawn when you die

let me know if you spot any problems

thanks for downloading this,

Increpare